# SubSpec

### Features:
It extends the Character Advancement panel with a button in the topright, names "SubSpec"
Once you click that, you can see the UI:

- New: Create a new profile and save your current skills and talents to that profile.
- Save: Save current profile to selected profile name. This will also try to create an ABS profile with the name like so `SpecID.Profilename`
- Load: Load a previous saved profile. This will try to restore ABS to the previously stored profile as well.
- Remove: Remove a profile.
- Rename: Rename a profile. This will also directly try to store the ABS profile under the updated name.
- Searchbox and button: Provide a search query. On enter or when button clicked, the profiles dropdown will be filtered.
- Spec dropdown: All profiles are stored under their own respective spec ID. You CAN however, import a profile from another spec.
- Profiles dropdown: Here you select the profile you want to load from or save to.
- Cost: Cost is currently hardcoded to `Scroll of Talent Unlearning`. Here it will show how many scrolls it costs to get from your current profile to the selected profile.

Future plans:
- Auto select the current profile from profiles dropdown when it matches(more clarity which profile is currently loaded).
- Add a talent loadout dropdown, as this is currently not integrated yet.
- Show which skills and talents will be unlearned and learned.
- Make the cost more accurate, actually accounting for how many scrolls you have, and showing required `Marks of Ascension`.
- Share builds privately with other people.
- If you have the book of ascension, get it out while loading a spec, so you have the highest rank spells.